#!/bin/bash

setup_dir=$(pwd)

echo "Setup is running from $setup_dir"

echo "[PTXDist] Downloading.."

wget http://public.pengutronix.de/software/ptxdist/ptxdist-2020.08.0.tar.bz2

echo "[PTXDist] Extracting.."

tar -xjf ptxdist-2020.08.0.tar.bz2 ptxdist-2020.08.0

echo "[PTXDist] Installing.."

cd ptxdist-2020.08.0
./configure && make -j24 && sudo make install -j24
ptxdist setup

echo "[OSELAS] Downloading source.."

wget https://public.pengutronix.de/oselas/toolchain/OSELAS.Toolchain-2020.08.0.tar.bz2

echo "[OSELAS] Extracting source.."

tar -xjf OSELAS.Toolchain-2020.08.0.tar.bz2 OSELAS.Toolchain-2020.08.0
cd OSELAS.Toolchain-2020.08.0
mkdir src
cd src 

# Download GCC from another mirror
echo "[GCC] Downloading GCC from a different mirror"
wget http://www.netgull.com/gcc/snapshots/10-20200822/gcc-10-20200822.tar.xz gcc-10-20200822.tar.xz

cd $setup_dir

echo "[APT] Installing packages"

sudo apt install -y liblzma-dev libmpfr-dev libisl-dev libgmp-dev libmpc-dev

echo "[PTXDist] Selecting configuration"
ptxdist-2020.08.0 select configs/ptxconfig

echo "[PTXDist] Buiding toolchain"
ptxdist-2020.08.0 go

echo "[PTXDist] Installing toolchain"

ptxdist make install
