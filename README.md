# Ebit EBAZ4205 bitcoin miner Linux 5.8 ptxdist buildroot

For more information, please checkout: https://github.com/xjtuecho/EBAZ4205


To get started run ./setup.sh WITHOUT sudo in a folder directly under ~/ (~/Zynq-Ebit for example).

# Wait for the thing to compile and install

Then run ./build.sh to build Linux and generate the various images

# Preparing your SD card

Format the SD card and create two partitions
## Partition 1

Format the first partition as FAT (FAT16), name it boot and give it a size of 40MB (just to have enough space)

Copy these files onto the boot partition

- linuximage
- u-boot.bin
- uenv.txt
- ebaz4205.dtb

MAKE SURE TO EJECT THE DRIVE!

## Partition 2

Format this partition as ext4, name it rootfs and extract the root.filesystem.tgz archive onto it.
This can be done with: (change the mount path for the sd card's second partition)

```bash
cd /media/root/rootfs
tar xvf root.filesystem.tgz .
sync
```

MAKE SURE THAT THE SYNC COMMAND HAS COMPLETED

# Booting
Insert the SD card into the board, connect a Serial to USB converter and open a Putty connection with it. The baudrate is 115200.
Insert the 12V plug and quickly press d on the Putty terminal.

U-Boot should now hold, if it doesn't and the Serial to USB converter disconnected, reconnect the USB cable to your computer and open a Putty session.
Quickly disconnect and reconnect the 12V plug into the board, to hope that the caps aren't fully discharged.
Again, press d to halt the boot process. (repeat if it failed again)

The default environment settings for the miner aren't the ones we'll be using.

Verify that your sd card is being recognized by U-Boot:
```bash 
zynq-uboot> mmcinfo
Device: zynq_sdhci
Manufacturer ID: 2
OEM: 544d
Name: SD01G
Tran Speed: 25000000
Rd Block Len: 512
SD version 1.0
High Capacity: No
Capacity: 982.5 MiB
Bus Width: 4-bit
Erase Group Size: 512 Bytes
zynq-uboot> ls mmc 0:1
            system volume information/
  4569760   linuximage
   548496   u-boot.bin
      396   uenv.txt
    10071   ebaz4205.dtb

4 file(s), 1 dir(s)

zynq-uboot> ls mmc 0:2
<DIR>       4096 .
<DIR>       4096 ..
<DIR>      16384 lost+found
<DIR>       4096 root
<SYM>          7 bin
<DIR>       4096 dev
<DIR>       4096 etc
<DIR>       4096 home
<SYM>          7 lib
<DIR>       4096 mnt
<DIR>       4096 proc
<DIR>       4096 run
<SYM>          8 sbin
<DIR>       4096 sys
<SYM>         13 system-update
<DIR>       4096 tmp
<DIR>       4096 usr
<DIR>       4096 var
```

In your u-boot shell copy/paste these commands:

```bash
setenv kernel_image 'linuximage'
setenv devicetree_image 'ebaz4205.dts'
setenv bootargs 'console=ttyPS0,115200 earlyprintk root=/dev/mmcblk0p2 rootfstype=ext4 rootdelay=6 noinitrd rw rootwait reboot=cold,hard emergency init=/bin/sh'
setenv sdboot_new 'if mmcinfo; then run uenvboot; echo Copying Linux from SD to RAM... && load mmc 0 ${kernel_load_address} ${kernel_image} && load mmc 0 ${devicetree_load_address} ${devicetree_image} && bootm ${kernel_load_address} - ${devicetree_load_address}; fi'
```

To actually boot into Linux (and into /bin/sh) run
```bash
run sdboot_new
```

The system should now boot.

# Getting access to all the peripherals

To fully enable the system, make sure to run

```bash
mount -a
```

# Boot log of successfull boot

```bash
zynq-uboot> run sdboot_new
Device: zynq_sdhci
Manufacturer ID: 2
OEM: 544d
Name: SD01G
Tran Speed: 25000000
Rd Block Len: 512
SD version 1.0
High Capacity: No
Capacity: 982.5 MiB
Bus Width: 4-bit
Erase Group Size: 512 Bytes
reading uEnv.txt
396 bytes read in 8 ms (47.9 KiB/s)
Loaded environment from uEnv.txt
Importing environment from SD ...
Copying Linux from SD to RAM...
reading linuximage
4569760 bytes read in 796 ms (5.5 MiB/s)
reading ebaz4205.dtb
10071 bytes read in 19 ms (517.6 KiB/s)
## Booting kernel from Legacy Image at 02080000 ...
   Image Name:   Linux-5.8.0
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    4569696 Bytes = 4.4 MiB
   Load Address: 00008000
   Entry Point:  00008000
   Verifying Checksum ... OK
## Flattened Device Tree blob at 02000000
   Booting using the fdt blob at 0x2000000
   Loading Kernel Image ... OK
   Loading Device Tree to 0f2fe000, end 0f303756 ... OK

Starting kernel ...

Booting Linux on physical CPU 0x0
Linux version 5.8.0 (ptxdist@ptxdist) (arm-v7a-linux-gnueabihf-gcc (OSELAS.Toolchain-2020.08.0 10-20200822) 10.2.1 20200822, GNU ld (GNU Binutils) 2.35) #5 SMP PREEMPT 2020-08-01T00:00:00+00:00
CPU: ARMv7 Processor [413fc090] revision 0 (ARMv7), cr=18c5387d
CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
OF: fdt: Machine model: Avnet ZedBoard board
Memory policy: Data cache writealloc
cma: Reserved 16 MiB at 0x0e000000
Zone ranges:
  Normal   [mem 0x0000000000000000-0x000000000fffffff]
  HighMem  empty
Movable zone start for each node
Early memory node ranges
  node   0: [mem 0x0000000000000000-0x000000000fffffff]
Initmem setup node 0 [mem 0x0000000000000000-0x000000000fffffff]
percpu: Embedded 15 pages/cpu s29772 r8192 d23476 u61440
Built 1 zonelists, mobility grouping on.  Total pages: 65024
Kernel command line: console=ttyPS0,115200 earlyprintk root=/dev/mmcblk0p2 rootfstype=ext4 rootdelay=6 noinitrd rw rootwait reboot=cold,hard emergency init=/bin/sh
Dentry cache hash table entries: 32768 (order: 5, 131072 bytes, linear)
Inode-cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
mem auto-init: stack:off, heap alloc:off, heap free:off
Memory: 232228K/262144K available (7168K kernel code, 183K rwdata, 1636K rodata, 1024K init, 130K bss, 13532K reserved, 16384K cma-reserved, 0K highmem)
rcu: Preemptible hierarchical RCU implementation.
rcu:    RCU event tracing is enabled.
rcu:    RCU restricting CPUs from NR_CPUS=4 to nr_cpu_ids=2.
        Trampoline variant of Tasks RCU enabled.
rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=2
NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
slcr mapped to (ptrval)
GIC physical location is 0xf8f01000
L2C: platform modifies aux control register: 0x72360000 -> 0x72760000
L2C: DT/platform modifies aux control register: 0x72360000 -> 0x72760000
L2C-310 erratum 769419 enabled
L2C-310 enabling early BRESP for Cortex-A9
L2C-310 full line of zeros enabled for Cortex-A9
L2C-310 ID prefetch enabled, offset 1 lines
L2C-310 dynamic clock gating enabled, standby mode enabled
L2C-310 cache controller enabled, 8 ways, 512 kB
L2C-310: CACHE_ID 0x410000c8, AUX_CTRL 0x76760001
random: get_random_bytes called from start_kernel+0x5a0/0x74c with crng_init=0
zynq_clock_init: clkc starts at (ptrval)
Zynq clock init
sched_clock: 64 bits at 333MHz, resolution 3ns, wraps every 4398046511103ns
clocksource: arm_global_timer: mask: 0xffffffffffffffff max_cycles: 0x4ce07af025, max_idle_ns: 440795209040 ns
Switching to timer-based delay loop, resolution 3ns
Console: colour dummy device 80x30
Calibrating delay loop (skipped), value calculated using timer frequency.. 666.66 BogoMIPS (lpj=3333333)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
CPU: Testing write buffer coherency: ok
CPU0: Spectre v2: using BPIALL workaround
CPU0: thread -1, cpu 0, socket 0, mpidr 80000000
Setting up static identity map for 0x100000 - 0x100060
rcu: Hierarchical SRCU implementation.
smp: Bringing up secondary CPUs ...
CPU1: thread -1, cpu 1, socket 0, mpidr 80000001
CPU1: Spectre v2: using BPIALL workaround
smp: Brought up 1 node, 2 CPUs
SMP: Total of 2 processors activated (1333.33 BogoMIPS).
CPU: All CPU(s) started in SVC mode.
devtmpfs: initialized
VFP support v0.3: implementor 41 architecture 3 part 30 variant 9 rev 4
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
futex hash table entries: 512 (order: 3, 32768 bytes, linear)
pinctrl core: initialized pinctrl subsystem
thermal_sys: Registered thermal governor 'step_wise'
NET: Registered protocol family 16
DMA: preallocated 256 KiB pool for atomic coherent allocations
cpuidle: using governor menu
hw-breakpoint: found 5 (+1 reserved) breakpoint and 1 watchpoint registers.
hw-breakpoint: maximum watchpoint size is 4 bytes.
zynq-pinctrl 700.pinctrl: zynq pinctrl initialized
e0001000.serial: ttyPS0 at MMIO 0xe0001000 (irq = 23, base_baud = 3125000) is a xuartps
printk: console [ttyPS0] enabled
vgaarb: loaded
usbcore: registered new interface driver usbfs
usbcore: registered new interface driver hub
usbcore: registered new device driver usb
usb_phy_generic phy0: supply vcc not found, using dummy regulator
mc: Linux media interface: v0.10
videodev: Linux video capture interface: v2.00
EDAC MC: Ver: 3.0.0
FPGA manager framework
Advanced Linux Sound Architecture Driver Initialized.
clocksource: Switched to clocksource arm_global_timer
NET: Registered protocol family 2
tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes, linear)
TCP established hash table entries: 2048 (order: 1, 8192 bytes, linear)
TCP bind hash table entries: 2048 (order: 2, 16384 bytes, linear)
TCP: Hash tables configured (established 2048 bind 2048)
UDP hash table entries: 256 (order: 1, 8192 bytes, linear)
UDP-Lite hash table entries: 256 (order: 1, 8192 bytes, linear)
RPC: Registered named UNIX socket transport module.
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
hw perfevents: no interrupt-affinity property for /pmu@f8891000, guessing.
hw perfevents: enabled with armv7_cortex_a9 PMU driver, 7 counters available
workingset: timestamp_bits=30 max_order=16 bucket_order=0
NFS: Registering the id_resolver key type
Key type id_resolver registered
Key type id_legacy registered
nfs4filelayout_init: NFSv4 File Layout Driver Registering...
Block layer SCSI generic (bsg) driver version 0.4 loaded (major 248)
io scheduler mq-deadline registered
io scheduler kyber registered
dma-pl330 f8003000.dmac: Loaded driver for PL330 DMAC-241330
dma-pl330 f8003000.dmac:        DBUFF-128x8bytes Num_Chans-8 Num_Peri-4 Num_Events-16
ehci_hcd: USB 2.0 'Enhanced' Host Controller (EHCI) Driver
ehci-pci: EHCI PCI platform driver
udc-core: couldn't find an available UDC - added [zero] to list of pending drivers
cdns-wdt f8005000.watchdog: Xilinx Watchdog Timer with timeout 10s
EDAC MC: ECC not enabled
Xilinx Zynq CpuIdle Driver started
sdhci: Secure Digital Host Controller Interface driver
sdhci: Copyright(c) Pierre Ossman
sdhci-pltfm: SDHCI platform and OF driver helper
mmc0: SDHCI controller on e0100000.mmc [e0100000.mmc] using ADMA
ledtrig-cpu: registered to indicate activity on CPUs
clocksource: ttc_clocksource: mask: 0xffff max_cycles: 0xffff, max_idle_ns: 537538477 ns
timer #0 at (ptrval), irq=37
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
fpga_manager fpga0: Xilinx Zynq FPGA Manager registered
NET: Registered protocol family 10
Segment Routing with IPv6
sit: IPv6, IPv4 and MPLS over IPv4 tunneling driver
Key type dns_resolver registered
Registering SWP/SWPB emulation handler
of-fpga-region fpga-full: FPGA Region probed
ALSA device list:
  No soundcards found.
Waiting 6 sec before mounting root device...
mmc0: new SD card at address d17e
mmcblk0: mmc0:d17e SD01G 983 MiB
 mmcblk0: p1 p2
EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)
VFS: Mounted root (ext4 filesystem) on device 179:2.
devtmpfs: mounted
Freeing unused kernel memory: 1024K
Run /bin/sh as init process
/bin/sh:
/ #: echo "hi"
hi
/ #:
```


# GPIO

To set the state of a GPIO pin of this board, make sure that zynq_gpio is displayed when executing
```bash
ls /sys/class/gpio/gpiochip906/label
```

Now calculate the pin number of the gpio controller, with 906 being the base value (of gpio 0)
To control pin 34, use 940 as the GPIO controlers pin.

## Export the GPIO pin

```bash
echo 940 > /sys/class/gpio/export
```

## Read the direction and the state of the pin

```bash
cat /sys/class/gpio/gpio940/direction
```

## Set the direction to output

```bash
echo out > /sys/class/gpio/gpio940/direction
```

## Set the pin state to 1
```bash
echo 1 > /sys/class/gpio/gpio940/value
```
